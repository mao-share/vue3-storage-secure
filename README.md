# vue3-storage-secure

## Getting Started

### Installation

```sh
# install with yarn
yarn add vue3-storage-secure

# install with npm
npm install vue3-storage-secure
```

### Vue version support
The main version Supports Vue 3.x only


## Usage

### The First Step
Register 
```js
import Vue3Storage from "vue3-storage-secure";

const app = createApp(App);
// 1. app.use(Vue3Storage)
// 2. app.use(Vue3Storage, { namespace: "jarvis_" })

// namespace: 命名空间，secureKey: 加密盐值
app.use(Vue3Storage, { namespace: "jarvis_", secureKey: "246810" })
```

### Teh Second step
use Global ComponentCustomProperties ```this.$storage```

```vue
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png" />
  </div>
</template>

<script lang="ts">
import { Options, Vue } from "vue-class-component";

@Options({
  components: {}
})
export default class Home extends Vue {
  created() {
    this.$storage.setStorageSync("test-key", "testdata");
  }
}
</script>

```

You can still use it like this:
````vue
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png" />
  </div>
</template>

<script lang="ts">
import { defineComponent } from "vue";
import { useStorage } from "vue3-storage-secure";

export default defineComponent({
  setup() {
    const storage = useStorage();

    storage?.setStorageSync("test-key", "testdata22");
    return {};
  }
});
</script>
````

You can still use it like this:
```vue
<script lang="ts">
import { defineComponent } from "vue";
import { useStorage } from "vue3-storage-secure";
import { CallbackResult } from "vue3-storage-secure/dist/lib/types";

export default defineComponent({
  setup() {
    const storage = useStorage();

    storage?.setStorage({
      key: "test-key",
      data: "testdata22",
      success: (callback: CallbackResult) => {
        console.log(callback.errMsg);
      }
    });
    return {};
  }
});
</script>
```

Use promise
```js
<script lang="ts">
import { defineComponent } from "vue";
import { useStorage } from "vue3-storage-secure";
import { CallbackResult } from "vue3-storage-secure/dist/lib/types";

export default defineComponent({
  setup() {
    const storage = useStorage();

    storage
      ?.setStorage({
        key: "test-key",
        data: "testdata22"
      })
      .then((successCallback: CallbackResult) => {
        console.log(successCallback.errMsg);
      })
      .catch((failCallback: CallbackResult) => {
        console.log(failCallback.errMsg);
      });

      const mKey = "mao";
      const mValue = "yupeng";
      console.log("set secure [" + mKey + "]：", mValue)
      storage?.setSecureStorageSync(mKey, mValue, 20)
      console.log("get secure key [" + mKey + "]：", storage?.getSecureStorageSync(mKey))
    
    return {};
  }
});
</script>

```

## Storage API

```ts
export interface StorageInterface {
    /**
     * Asynchronous storage
     * @param option
     */
    getStorage<T = any>(option: GetStorageOption<T>): Promise<GetStorageSuccessCallbackResult<T>>;
    
    /**
     * Synchronous storage
     * 
     * @param key 
     *
     */
    getStorageSync<T = any>(key: string): T | undefined;

    getSecureStorageSync<T = any>(key: string): T | undefined;
    
    /**
     * Synchronously obtain the storage content of the corresponding key
     *
     * @param key
     * @param data 
     * @param expire
     */
    setStorageSync(key: string, data: any, expire?: number): void;

      /**
       * 同步设置加密的存储内容
       * @param key 本地缓存中指定的 key
       * @param data 需要存储的内容。只支持原生类型、Date、及能够通过`JSON.stringify`序列化的对象。
       * @param expire 失效时间
       */
      setSecureStorageSync(key: string, data: any, expire?: number): void;
    
    /**
     * Asynchronously obtain the storage content of the corresponding key
     *
     * @param option
     */
    setStorage(option: SetStorageOption): Promise<CallbackResult>;
    
    /**
     * Determine whether the data has expired
     * @param key
     */
    isExpire(key: string): boolean;
    
    /**
     * Correspondingly obtained key name index
     * @param index
     */
    key(index: number): string | null;
    
    /**
     * Determine whether the key name exists
     *
     * @param key
     */
    hasKey(key: string): boolean;
    
    /**
     * Asynchronously remove the specified key from the local cache
     *
     * @param option
     */
    removeStorage(option: RemoveStorageOption): Promise<CallbackResult>;
    
    /**
     * Synchronously remove the specified key from the local cache
     *
     * @param name
     */
    removeStorageSync(name: string): void;
    
    /**
     * Get current storage information asynchronously
     *
     * @param option
     */
    getStorageInfo(option?: GetStorageInfoSuccessCallbackOption): Promise<CallbackResult>;
    
    /** Get current storage information synchronously */
    getStorageInfoSync(): GetStorageInfoOption;
    
    /**
     * Clean up local data cache asynchronously
     * @param option
     */
    clearStorage(option?: ClearStorageOption): Promise<CallbackResult>;
    
    /**
     * Synchronously clean the local data cache
     */
    clearStorageSync(): void;
   
    /**
     * Set storage namespace
     * @param namespace
     */
    config(namespace?: string): void;
}
```

## ⚖️ License

MIT
