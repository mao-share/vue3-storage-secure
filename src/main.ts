import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import Vue3Storage from "../packages/index";

createApp(App)
  .use(router)
  .use(Vue3Storage, { namespace: "jarvis_", secureKey: "246810" })
  .mount("#app");
